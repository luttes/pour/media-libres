.. index::
   ! Climate justice

.. raw:: html

    <a rel="me" href="https://qoto.org/@pvergain"></a>
    <a rel="me" href="https://framapiag.org/@pvergain"></a>
    <a rel="me" href="https://qoto.org/@climatejustice"></a>

.. figure:: images/greta.png
   :align: center
   :width: 300

   https://mastodon.nu/@gretathunberg, https://fr.wikipedia.org/wiki/Greta_Thunberg

.. _climate_justice:
.. _justice_climatique:

===========================================
**Luttes pour les media libres**
===========================================

- https://fr.wikipedia.org/wiki/Justice_climatique
- https://en.wikipedia.org/wiki/Climate_justice


La notion de **justice climatique** désigne les approches éthiques, morales,
de justice et politique de la question de l'égalité face au dérèglement
climatique, plutôt que les approches uniquement environnementales ou physiques.

Elle est aussi utilisée pour désigner les plaintes et actions juridiques
déposées pour action insuffisante contre le changement climatique et
pour l'adaptation.

Entre 2006 et 2019, plus de 1 300 plaintes relatives au climat ont été
déposées dans une trentaine de pays, parfois par des enfants.

Les accusés étaient surtout des gouvernements, mais ces actions en
justice visent de plus en plus de grandes entreprises fortement émettrices
de gaz à effet de serre.



.. toctree::
   :maxdepth: 3

   orgas/orgas
   humour/humour

.. toctree::
   :maxdepth: 5

   news/news
