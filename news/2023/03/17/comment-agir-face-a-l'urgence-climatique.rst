
.. _agir_2023_03_17:

==========================================================
**2023-03-17 Comment agir face à l'urgence climatique**
==========================================================

Cette vidéo est extraite d'un débat qui a eu lieu le 17 mars au @fifdh
sur le thème "(In)action climatique : comment agir face à l'urgence ?"
avec @JKSteinberger et @cdion.

Une discussion plus que jamais d'actualité à revoir ici: https://www.youtube.com/watch?v=lLTCP2mNSbw
