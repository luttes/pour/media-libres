
.. _giec_2023_03_20:

=================================================================
**2023-03-20 Le giec sort la synthese de 8 ans de travail**
=================================================================

- https://www.ipcc.ch/report/ar6/syr/
- https://bonpote.com/rapport-de-synthese-du-giec-chaque-dixieme-de-degre-compte/
- https://vert.eco/articles/les-11-points-clefs-de-lultime-rapport-du-giec-synthese-dun-monde-en-fusion
- https://www.heidi.news/climat/giec-voici-le-plan-de-sortie-de-la-crise-climatique-a-l-intention-des-decideurs
- https://www.blast-info.fr/articles/2023/le-giec-sort-la-synthese-de-8-ans-de-travail-un-guide-de-survie-pour-lhumanite-et-tout-le-monde-sen-fout-ou-presque-g5PA8090TC25fURfOnMWHw


- http://nitter.hostux.net/antonioguterres/status/1637818895623942146#m

The climate time-bomb is ticking but the latest @IPCC_CH report shows
that we have the knowledge & resources to tackle the climate crisis.

We need to #ActNow to ensure a livable planet in the future. ipcc.ch/report/ar6/syr/


#IPCC #ClimateChange 2023: Synthesis Report "is a survival guide for humanity,"
says @‌UN SG @‌antonioguterres in the IPCC press conference today.

Follow live 👉 bit.ly/SYRPRcfe


Ne demandez plus au GIEC plus d’évidences. Attendre le 7e rapport sera trop tard, vous le savez!
=================================================================================================

- http://bird.trom.tf/cassouman40/status/1637934022792716289#m
- https://mastodon.xyz/@geekenvrac/110061247411625199


Le 6e cycle #GIEC est clos. 8 ans de travail acharné de 100aines d'auteur-es.
Les faits sont clairs, ne peuvent être +clairs. Gouvernements, décideurs,
maintenant c'est a vous d'agir!

Ne demandez plus au GIEC plus d’évidences. Attendre le 7e rapport sera
trop tard, vous le savez!


Merci de ne pas détourner les évaluations #GIEC
==========================================================

- https://twitter.com/cassouman40/status/1638122804062179328

Christophe Cassou : "@davidlisnard Sincèrement, étant auteur principal
du 6e rapport GIEC, je peux vs affirmer que la position du curseur entre
espoir et désespoir penche franchement vers désespoir...

Merci de ne pas détourner les évaluations #GIEC (Cf fig.7 du SYR explicite).
Ns sommes en pleine sortie de route!


Paloma Moritz
===============

- https://nederland.unofficialbird.com/PalomaMoritz/status/1637728523036114945#m
- https://www.blast-info.fr/articles/2023/le-giec-sort-la-synthese-de-8-ans-de-travail-un-guide-de-survie-pour-lhumanite-et-tout-le-monde-sen-fout-ou-presque-g5PA8090TC25fURfOnMWHw


Le 49.3 cette année, mais cela avait aussi été le cas lors de la sortie
des précédents volets avec le transfert de Lionel Messi au PSG ou encore
la guerre en Ukraine: la répétition de l’invisibilité médiatique des
rapports du GiEC commence à peser lourd.

Pourtant ces travaux devraient être diffusés sur tous les plateaux télé
et rendus accessibles au plus grand nombre car leur prise en compte
déterminera nos conditions de vie à moyen terme et la survie de l’humanité à long terme.

Cette synthèse, rédigée par 93 scientifiques, est la version la plus à
jour de l’état des connaissances sur la science climatique.

Elle a pour objectif d’éclairer les Etats dans leurs actions face à
l’urgence (vaste projet).

Elle résume à la fois les trois volets de son 6ème rapport mais aussi
les trois rapports spéciaux sur les conséquences d’un réchauffement de
1,5°C, sur les terres et sur les océans (et la cryosphère).

La conclusion de cette synthèse est attendue : si les émissions de gaz
à effet de serre ne sont pas réduites immédiatement et de façon drastique
dans tous les secteurs, conserver un monde vivable pour toutes et tous
deviendra quasiment impossible.


Comment agir face à l'urgence ?" avec @JKSteinberger et @cdion
=================================================================

Cette vidéo est extraite d'un débat qui a eu lieu le 17 mars au @fifdh
sur le thème "(In)action climatique : comment agir face à l'urgence ?"
avec @JKSteinberger et @cdion.
Une discussion plus que jamais d'actualité à revoir ici https://www.youtube.com/watch?v=lLTCP2mNSbw





- https://mastodon.world/@ManuPatola/110061099741475036
- https://www.ledevoir.com/environnement/786019/l-humanite-brule-ses-dernieres-chances-de-limiter-le-rechauffement-climatique

« Malgré l’urgence précisée un peu plus à chaque rapport du #giec les
fonds publics et privés destinés aux énergies fossiles sont toujours + élevés
que ceux prévus pour la lutte contre la crise climatique et l’adaptation à
ses impacts.

Pour 2022, l’Agence internationale de l’énergie a évalué les subventions aux ressources fossiles à + de 1000 milliards de dollars. »


Tant qu'il s'adressera "aux décideurs" et proposera des "solutions" à l'intérieur du cadre idéologique qui est la cause du réchauffement climatique, le GIEC ne fera que ponctuer les décennies d'inaction
==============================================================================================================================================================================================================

- https://piaille.fr/@Pr_Logos/110056017223356360
- https://www.heidi.news/climat/giec-voici-le-plan-de-sortie-de-la-crise-climatique-a-l-intention-des-decideurs

Tant qu'il s'adressera "aux décideurs" et proposera des "solutions" à
l'intérieur du cadre idéologique qui est la cause du réchauffement
climatique, le GIEC ne fera que ponctuer les décennies d'inaction.




