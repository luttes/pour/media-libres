.. index::
   pair: Climate change mitigation; 2022-11-30

.. _climate_change_mitigation_2022_11_30:

===========================================================================================================================================
**2022-11-30 Climate change mitigation: time for action** by Céline Guivarch (IPCC AR6 Working group 3: Mitigation of Climate Change)
===========================================================================================================================================

- https://ingenius.ecoledesponts.fr/en/articles/climate-change-mitigation-time-for-action/
- :ref:`ipcc_reports_ar6_group_1`
- :ref:`ipcc_reports_ar6_group_2`
- :ref:`ipcc_reports_ar6_group_3`


Introduction
===============

With its 3,000 pages, over 18,000 peer-reviewed academic papers, 278 lead
authors and thousands of contributors, the :ref:`IPCC report published in April 2022 <ipcc_reports_ar6_group_3>`
represents the most up-to-date scientific knowledge on climate change mitigation.

In case you haven't had time to read the extensive report, its key
messages are summed up here, in the hope that it will encourage you
to go further.


In 2021, :ref:`the first part of the IPCC report <ipcc_reports_ar6_group_1>`, on the physical basis of climate
change, established the human impact on global warming as an unequivocal
fact: 1.1°C above pre-industrial levels, **the highest level in 100,000 years**.

A few months later, :ref:`the second part of the report <ipcc_reports_ar6_group_2>`
showed the ripple effects of this warming on ecosystems and human societies.

It also focused on solutions for adapting to a changing climate, but
also the limitations of these solutions.

The :ref:`third part of the report <ipcc_reports_ar6_group_3>` builds on these findings, summarizing scientific
knowledge on past emissions and the outlook for future emissions, options
for reducing emissions, climate change policies, the financing and the
innovations they require. What does this report tell us ?


A critical decade (2020-2030)
================================

Despite the growing number of policies and measures, in numerous countries
and at all levels of government, greenhouse gas (GHG) emissions remain high.

These policies and measures have helped prevent emissions, but only slowed
their rate of growth at the global level, without reversing the trend.

As such, GHG emissions, which accumulate in our atmosphere and cause
climate change, have never been higher in absolute terms.


Yet, in order to pursue a pathway compatible with limiting global warming
to 2°C, global emissions must be reduced by 20 to 30% by 2030, compared
to their level in 2019. And to limit warming to 1.5°C, they must be cut in half.

This means that the present decade is critical to initiate and accelerate
a reduction in global GHG emissions, and set in motion the transformations
needed in key sectors – energy, industry, transport, building, infrastructure,
cities, the agri-food systems, among others.

Regardless of the targeted level for global warming stabilization, we must
achieve net zero CO2 emissions (also known as CO2 neutrality or carbon neutrality),
and reduce other GHGs – methane, nitrous oxide, fluorinated gases.

In pathways compatible with 1.5°C, this carbon neutrality is to be achieved
around 2050, and around 2070 for 2°C pathways.


Limiting the extent of climate change
==========================================

While major transformations are needed in all key sectors, solutions are available.

There are options for reducing emissions that cost less than $100 per
ton of CO2, which could cumulatively halve global GHG emissions by 2030.

Such options include technologies for generating renewable (solar, wind)
or nuclear electricity, solutions for reducing the leakage of methane
and fluorinated gases, storing carbon in agricultural soils, restoring
ecosystems and forests, solutions for energy conservation and efficiency
in buildings, developing public transport and active mobility (cycling
and walking), the electrification of vehicles, solutions for using energy
and materials efficiently in industry, and recycling.

The report focuses especially on actions related to demand (for energy,
materials, goods, land, water) and highlights the potential to significantly
reduce emissions – by 40-70% – by 2050. Some of these actions are based
on adopting technologies, while others involve efficiency and sufficiency.

Examples of such actions in the building sector include:

- installing solar panels on roofs (technology)
- insulation (efficiency)
- setting heating to 19°C instead of 20°C (sufficiency)
- In the transportation sector, actions are based on an "avoid, shift
  improve" model. For example:

  - avoiding travel needs, through urban planning to shorten distances
    between homes, workplaces, schools, shops etc.

  - avoiding the movement of goods by shortening supply chains, shifting
    from high-emission (planes, cars) to low-emission (rail, public
    transportation) or zero-emission (cycling, walking) modes of transportation

  - improving vehicles by making them light, electric etc.

The report underscores that these demand-side actions are not the responsibility
of end users alone, but require collective action and public policies
to transform infrastructure, and build and enable access to carbon-free
alternatives for everyone.

Cities on the front lines
===========================

The report also focuses on cities, which concentrate people and activities,
and therefore emissions, but also offer ways to reduce them: urban planning,
buildings, transportation networks, improving carbon uptake and storage
(by adding trees, vegetation, green spaces, ponds).

The latter option is also a way to adapt to urban heat island effects
during heat waves, which become more frequent with every fraction of a
degree of warming.

The costs of a number of these options for reducing emissions have dropped
significantly with their recent deployment (for example, the cost of
solar energy has decreased tenfold over the past decade).

Moreover, it has become clear that the macroeconomic cost of action to
reduce emissions is lower than the cost of inaction: taking action to
limit the extent of climate change costs less than suffering from the
damage it causes.

However, the report shows that the major transformations needed to reduce
emissions cannot be carried out on an incremental basis and highlights
the barriers to initiating these transformations.


Barriers to climate change mitigation
==========================================

**Some barriers are physical in nature**.

For example, if already-existing fossil fuel-related infrastructure (coal,
gas or oil-fired power plants, industrial facilities, internal combustion
vehicles etc.) is used until the end of its technical lifetime, this
would exceed the emissions budget consistent with 1.5°C warming.

**Other barriers are financial**.

Depending on the sector and region, the investments observed are 3 to 6
times lower than the level of investment needed for mitigation.

The investment gaps are the widest in developing countries, and in the
transportation, agriculture and forestry sectors.

However, there is sufficient capital available globally to close these gaps.
This would require redirecting investment, which still overwhelmingly
finances fossil fuels, towards mitigation solutions.

**Still other barriers are institutional**.

The magnitude of the transformations that must be carried out call for
ambitious, coordinated action between all levels of government – from
territories, regions, and countries to the European and international level –
in an extremely unequal world.

At the global level, the richest 10% of countries are responsible for
roughly 40% of emissions, while the poorest 50% are responsible for
less than 15% of emissions.

**International solidarity is needed** to respond to the issues of climate
change, technology transfer and international funding of actions to
reduce emissions.

Exploring potential synergies
=================================

To help overcome these barriers, the report explores potential synergies
with other development goals (such as eradicating poverty, improving health,
education, and reducing inequality).
Multiple synergies are possible in the area of health, for example.
Limiting the burning of fossil fuels reduces greenhouse gas emissions
and also reduces local pollutants that are harmful to health.

Developing active forms of mobility reduces emissions and combats physical
inactivity. Eating a diet with less meat, in keeping with nutritional
guidelines, keeps people healthy and reduces methane emissions.

The report also highlights approaches to a "just" transition, allowing
everyone to find their place in a future low-carbon world.

These approaches take into account the issue of inequality, from the
mitigation policy design stage. They involve education, professional
retraining, and collectively constructed  alternatives.

Preventing the worst effects
=================================

The evidence is clear: urgent action is needed to reduce emissions and
prevent the worst effects of climate change.
Each additional fraction of a degree gives rise to additional risks and
damage.

The solutions for reducing greenhouse gas emissions are well-known.

We have the tools to implement these solutions, while improving well-being
for all.

We are on the cusp of decades of extensive action to radically transform
all major systems and achieve net zero CO₂ emissions.
And this decade is critical to set these changes in motion. 

What role will you play in these transformations ?
========================================================

This is a real question.






